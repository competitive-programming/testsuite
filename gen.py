from random import *

# Generate a random array of **INTEGERS** with elements in the range [A, B]
def genRandomArray(N, A, B):
    arr = [randrange(A, B + 1) for _ in range(N)]
    return arr;


# Generate random array of **REAL** with elements in the range [a, b]
def genRandomArrayReal(N, A, B):
    arr = [uniform(A, B + 1) for _ in range(N)]
    return arr;


# Generate random matrix of size N*N with random integers
'''
# print matrix with space b/w numbers in range [A,B]
    for row in matrix:
        print(' '.join(map(str, row)))
'''


def genRandomMatrix(N, A, B):
    for r in range(N):
        m = [randint(A, B) for c in range(N)]
    return m;


# with zero at diagonal element
def genRandomZDMatrix(N, A, B):
    for r in range(N):
        m = [randint(A, B) if r != c else 0 for c in range(N)]
        return m;


# symmetric matrix
def genRandomSymMatrix(N, A, B):
    symmatrix = [[0] * N for _ in range(N)]
    for r in range(N):
        for c in range(r + 1):
            symmatrix[r][c] = symmatrix[c][r] = randint(A, B)
    return symmatrix;


# Generate a random string from characters in the range [a, b]
def genRandomString(N, A, B):
    l = genRandomArray(N, ord(A), ord(B))
    s = ''
    for char in l: s += chr(char)
    return s;


# Generate a random permutation of [1, 2 ... N]
def genRandomPermutation(N):
    permutation = list(range(1, N + 1))
    shuffle(permutation)
    return permutation;


# Generate a random unweighted tree
def genRandomTree(N):
    edges = []
    for u in range(2, N + 1):
        v = randrange(1, u)
        edges.append([u, v])

    permutation = genRandomPermutation(N)

    for i in range(0, N - 1):
        u, v = edges[i]
        u = permutation[u - 1]
        v = permutation[v - 1]
        edges[i] = (u, v)
    return edges;


# Generate a random weighted tree
def genRandomWeightedTree(N, L, R):
    weigths = genRandomArray(N - 1, L, R)
    tree = genRandomTree(N)
    wtree = []

    for i in range(0, N - 1):
        u, v, w = tree[i][0], tree[i][1], weigths[i]
        wtree.append((u, v, w))

    return wtree;


# Undirected, no multiedges and no self-loops
def genRandomGraph(N, E):
    edges = {}

    if N == 1: return []

    for i in range(E):
        u = randrange(1, N + 1)
        v = u
        while v == u: v = randrange(1, N + 1)

        while (u, v) in edges or (v, u) in edges:
            u = randrange(1, N + 1)
            v = u
            while v == u: v = randrange(1, N + 1)

        edges[(u, v)] = 1

    ret = []
    for edge in edges: ret.append(edge)

    return ret;


# Undirected, no multiedges, no self-loops, connected
def genRandomConnectedGraph(N, E):
    E -= N - 1
    tree = genRandomTree(N)
    edges = {}
    for edge in tree:
        edges[edge] = 1

    for i in range(E):
        u = randrange(1, N + 1)
        v = u
        while v == u: v = randrange(1, N + 1)

        while (u, v) in edges or (v, u) in edges:
            u = randrange(1, N + 1)
            v = u
            while v == u: v = randrange(1, N + 1)

        edges[(u, v)] = 1

    ret = []
    for edge in edges: ret.append(edge)

    return ret;


# Undirected, no multiedges, no self-loops, can be forced to be connected
def genRandomWeightedGraph(N, E, L, R, connected=False):
    graph = []
    if not connected:
        graph = genRandomGraph(N, E)
    else:
        graph = genRandomConnectedGraph(N, E)

    weights = genRandomArray(E, L, R)

    wgraph = []
    for i in range(E):
        u, v, w = graph[i][0], graph[i][1], weights[i]
        wgraph.append((u, v, w))
    return wgraph;


if __name__ == '__main__':
    # Start from here



    # print(genRandomArray(10, 1, 1000))
    # print(genRandomArray(10, 1, 1000))
    # print(genRandomMatrix(9, 1, 10))
    # print(genRandomZDMatrix(9, 1, 10))
    # print(genRandomSymMatrix(3, 1, 10))
    # print(genRandomString(10, '0', '1'))
    # print(genRandomPermutation(100))
    # print(genRandomTree(10))
    # print(genRandomWeightedTree(10, 1, 10))
    # print(genRandomGraph(10, 5))
    # print(genRandomConnectedGraph(10, 5))
    # print(genRandomWeightedGraph(10, 5, 1, 10, False))
    

    sys.exit(0)
