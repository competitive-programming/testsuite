import sys
import os

def info():
    # print (__doc__)
    print ('Platform: ' + sys.platform + '.')
    print ('Python: %s, located at %s.' % (sys.version[:5], sys.executable))


if __name__ == '__main__':
    info()

    sys.exit(0)
