#!/bin/bash

pythonx=`which python3`
testcase=0
fail="False"

while [ $testcase -lt 10000 ]
do

	testcase=`expr $testcase + 1`
	$pythonx gen.py > testcase.in
	./A < testcase.in > A.out
	./brute < testcase.in > B.out
	result=$($pythonx check.py)

	if [ "$result" == "$fail" ]
	then
		echo -e "\n\e[31;1mWrong Answer! at ${testcase}:\e[0;m"
		cat testcase.in
		echo -e "\e[32;1m=> Correct Output : \e[0;m"
		cat B.out
		echo -e "\e[31;1m=> Incorrect Output : \e[0;m"
	    cat A.out
		break
	fi
    echo -en "\e[32;1m" $testcase "\e[0m"
done

# echo -en "TestCase : " $testcase " = " $result
#echo -e "\033[31;1m* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\033[0;m"